Orange lessons are guided projects. These take you through the implementation of a program step-by-step, using concepts from prior lessons.

Solution for Pyflo:
1. [Flag Creator](https://nekobin.com/fojezuvaxi)
2. [Finances Visualizer](https://nekobin.com/ziqojegeca)