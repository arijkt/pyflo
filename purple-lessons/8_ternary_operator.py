a, b = 10, 20

min = a if a < b else b 

print(min)


a, b = 20, 30

if a != b:
    if a > b:
        print('a is greater than b')
    else:
        print('b is greater than a')
else:
    print('both a and b are equal')

a, b = 20, 30
print('Both a and b are equal' if a == b else 'a is greater than b' if a > b else 'b is greater than a')