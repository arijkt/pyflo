# What is a Computer

Essentially, a computer is not defined by its physical components, but rather by what it is capable of accomplishing and computing. Many years ago, a scientist named Alan Turing conceived the idea of what is now known as a "Turing Machine" - a mathematical model for how a computer can solve problems in an organized, step-by-step manner. This model is very simple compared to modern digital computers, yet it can simulate any computer algorithm or program we use today.  

The model created by Turing can be considered as a hypothetical machine that has an infinitely long "tape" containing slots for 1 bit of information (1 or 0). In addition to the tape, this machine also has a needle (or head) that points to 1 bit of information at a time. At each step of operation, the machine can choose to:

- Change the symbol on the tape pointed at by the head  
- Move the head one tape slot to the right
- Move the head one tape slot to the left

This machine could then be given a sequence of instructions with a set of states that determine how the machine will operate and what problem it will work on solving. The instructions and states constitute a "program" that controls what the head and tape will do, and how decisions are made.

[cd ..](./README.md)
