# PEMDAS stands for "Parentheses, Exponents, Multiplication, Division, Addition, Subtraction" 

x = 100 + 20 * 2
# in python, when you divide an integer by another integer, it returns a float
y = 200 - 100 / 2
print(x, y) # output: 140, 150.0
print(type(x)) # <class 'int'>
print(type(y)) # <class 'float'>

a = (40 / 2 + (2 * 5 ** 3))
print(a) # output: 270.0
print(type(a)) # output: <class 'float'>

count = 100
pi = 3.14
print(type(count)) # output: <class 'int'>
print(type(pi)) # output: <class 'float'>