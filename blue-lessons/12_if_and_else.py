# Whenever the first condition is true, the other is guaranteed to be false mathematically. 
# Whenever the first condition is true, the other is guaranteed to be false mathematically.
earned = int(input('How much money did you earn? '))
spent = int(input('How much money did you spend? '))
if earned >= spent:
    print('You didn\'t overspend :)')
else:
    print('You did overspend :(')
    
    
# Examples
# A program to tell you if you can join the US army.
age = int(input('Enter your age: '))
if age >= 18:
    print('You may join the US army.')
else: 
    print('You cannot join yet.')


# A program to tell you if you should pay your rent or not.
day = int(input("What day in the month is it? "))
if day <= 20:
    print("You have plenty of time")
    print("No need to pay your rent yet")
else:
    print("The end of the month will be here soon")
    print("Pay your rent ASAP")
    
    
# A program to check if a name is valid based on length and capitalization.
name = input("What is your first name? ")
if len(name) < 35 and name[0].isupper():
    print('Thats a valid name')
else:
    print("Sorry, your name is either")
    print("  * Too long")
    print("  * Not capitalized")