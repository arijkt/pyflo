# Binary Numbers

Computers use binary, a sequence of 1s and 0s, to represent code and data. This limited representation allows computers to represent various types of data, such as decimal numbers, text, and images, in a way that is easily understandable by humans. The decimal number system, which comes from the Greek word "deka" meaning "ten," has ten unique digits for representing numbers (0, 1, 2, 3, 4, 5, 6, 7, 8, 9) and can be combined to represent quantities of money or items. For small values, a single digit can be used, while larger values require adding a second digit.

Binary is another number system similar to decimal numbers, with the "Bi" in binary meaning "two." Instead of having ten digits, only two are needed. The principles of counting, numbers growing, and math follow the same exact patterns as decimal numbers, but may seem odd to those who have never used binary before. The first ten decimal numbers with the corresponding binary are listed below.

- 0 = 0
- 1 = 1
- 2 = 10
- 3 = 11
- 4 = 100
- 5 = 101
- 6 = 110
- 7 = 111
- 8 = 1000
- 9 = 1001
- 10 = 1010
- . . . .

Notice how the binary numbers change. After using 0 and 1, we have already exhausted all of the possible 1-digit numbers. Thus, in order to represent higher values, we need to jump to two digits to represent the decimal values 2 and 3 (10 and 11). We’ve only reached 3, but yet again we are at an exhaustion point. The only way to represent higher values is to jump to three digits, 100 for 4, 101 for 5, and so on.

Operations such as addition and subtractions again function the same, but with less digits to work with. Imagine you were to add the decimal numbers 15 and 87 to get 102. When adding these, you would add the first two digits (5+7 = 12). At this point, you’d know the resulting number will have 2 as the least-significant digit, and the 1 would carry. We then add 1+1+8 = 10. Thus, the final result is 102.

![decimal-addition](https://pyflo.net/binary-numbers/decimal-addition.gif)

In binary addition is the same set of steps. When adding 1011 (eleven) and 1110 (fourteen) we would do these steps:

![binary-addition](https://pyflo.net/binary-numbers/binary-addition.gif)

- Sum first digits, 1 + 0 = 1. Result = XXXX1
- Sum second digits, 1 + 1 = 10 (2). Result = XXX01 with a 1 carried.
- Sum third digits, 1 + 0 + 1 (carried over) = 10 (2). Result = XX001 with a 1 carried.
- Sum fourth digits, 1 + 1 + 1 (carried over) = 11. Result = X1001 with a 1 carried
- With the 1 carried from last round, final result = 11001

Humans think in decimal and computers “think” in binary. Due to this, a useful skill for programmers to develop is the ability to quickly convert between decimal and binary numbers. One approach to this would be to count from zero up to N whenever you need to do the conversion to some value N. This would work well for small numbers (say, less than 20 or 30), but after that this would start to become very unruly. What would you do if you need to convert the “503 square feet” to binary? That’s a lot of counting. A better procedure for completing these conversions is presented in the next lesson.

[cd ..](./README.md)
