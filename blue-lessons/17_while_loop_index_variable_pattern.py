index = 0          # PART 1: Declare an "index" variable
while index < 10:  # PART 2: Compare index to a value in the condition
    print("hi")    # PART 3: The code to run (can be multiple lines)
    index += 1     # PART 4: Add to or subtract from the index

# For program A, we know that we have a situation where we want something to repeat 1000 times. Thus, we can start by just writing the general outline for a loop to repeat 1000 times, based on our pattern:
number_sum = 0
index = 1
while index < 6:
    number_sum += index
    index += 1

print('The sum is:', number_sum, index)

'''
1 < 6

number_sum          index
0 = 0 + 1 = 1       1 = 1 + 1 = 2
1 = 1 + 2 = 3       2 = 2 + 1 = 3
3 = 3 + 3 = 6       3 = 3 + 1 = 4
6 = 6 + 4 = 10      4 = 4 + 1 = 5
10 = 10 + 5 = 15    5 = 5 + 1 = 6
'''

# For program B, we want to print out a pyramid, something along the lines of this:
pyramid_size = 10
i = 1 # index = 1
while i <= pyramid_size:
    spacing = pyramid_size - i
    print(" " * spacing + '#' * (i * 2))
    i += 1

'''
1 <= 10

spacing      # (hash)
10 - 1 = 9   1 * 2 = 2             ##
9 - 1 = 8    2 * 2 = 4            ####
8 - 1 = 7    3 * 2 = 6           ######
7 - 1 = 6    4 * 2 = 8          ########
6 - 1 = 5    5 * 2 = 10        ##########
5 - 1 = 4    6 * 2 = 12       ############
4 - 1 = 3    7 * 2 = 14      ##############
3 - 1 = 2    8 * 2 = 16     ################
2 - 1 = 1    9 * 2 = 18    ##################
1 - 1 = 0    10 * 2 = 20  ####################
'''