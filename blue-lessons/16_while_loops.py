word = input('Gimme a word: ')
upper_word = word.upper()
print('The word in upper-case:', upper_word)

# In python, there are two main types of loops: while-loops and for loops. 
# How to we put this into practice to repeat the word upper-case example three times? As with many programs, there’s a number of ways it could be solved. Perhaps the simples way would be:
repeat_count = 1
while repeat_count <= 3:
    repeat_count += 1 
    word = input('Gimme a word: ')
    upper_word = word.upper()
    print('The word in upper-case:', upper_word)