i = 0
while i < 10:
    j = 0
    while j < 10:
        print('hi!')
        print(j)
        j += 1
    i += 1
print('done!')

# Now, instead of printing out the entire string, we want to print out every 10-letter subsequence from within it. For example:
sentence = 'There once lived a bee in a house by the sea'
index = 0 
while index < 10:
    print(sentence[index], end='')
    index += 1
print()
print()

'''
Now, we want to repeat the same thing over-and-over, 
except we want to “shift” over the starting position of each string to the right by one letter for each print. 
To do this, we should put the loop we wrote inside of another loop, 
that iterates through the position of the sentence string, 
but stop 10 short of the end.
'''

sentence = "*** **** ***** ******"
position = 0
while position < len(sentence) - 10:
    index = position
    while index < position + 10:
        # print("aseek")
        print(sentence[index], end='')
        index += 1
    print()
    position += 1

'''
Output:

*** **** *
** **** **
* **** ***
 **** ****
**** *****
*** ***** 
** ***** *
* ***** **
 ***** ***
***** ****
**** *****
'''