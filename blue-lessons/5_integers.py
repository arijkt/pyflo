name = 'Pierre'
email = 'pierre@gmail.com'
age = 50
height = 72

print('His name is ' + name)
print(name + ' can be contacted at ' + email)
# Create a new string object from the given object
print('He is ' + str(age) + ' years old and is ' + str(height)   + ' inches tall')