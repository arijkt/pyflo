# In order to do this we will need a loop to iterate through the characters of the string and an if/else to determine whether or not a letter is upper or lower case. We first need to get an input string from the user:
text = input('Type some text and then press ENTER: ')

uppercase_count = 0
lowercase_count = 0
for index in range(0, len(text)):
    char = text[index]
    if char.isupper():
        uppercase_count += 1
    elif char.islower():
        lowercase_count += 1

print('Number of upper-case letters:', uppercase_count)
print('Number of lower-case letters:', lowercase_count)


# we should ask the user to give us the starting and ending range through which to check for numbers divisible by 7.
start = int(input('What value should we start at? '))
end = int(input('What value should we end at? '))
divide = int(input('What number to check divisiblility for? '))

number = start

while number <= end:
    if number % divide == 0:
        print(number, 'is divisible by', divide)
    number += 1