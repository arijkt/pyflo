Purple lessons are part of a learning branch. These have great material, but can be skipped over if desired. Bonus content!

### Table of Content

- [What is a Computer](./1_what_is_a_computer.md)
- [Computer Internals](./3_code_and_data.md)
- [Code and Data](./3_code_and_data.md)
- [Binary Numbers](./4_binary_numbers.md)
- [Converting to and from Binary](./5_converting_to_and_from_binary.md)
- [Representing Text in Binary](./6_representing_text_in_binary.md)