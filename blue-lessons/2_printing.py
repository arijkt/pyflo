print('the big black box')
print('the\nbig\nblack\nbox')

# i tell the print func to put a space (' ') rather than a newline ('\n') at the end instead
print('the', end=' ')
print('big', end=' ')
print('black', end=' ')
print('box', end=' ')

# we can specify multi-line strings if we begin and end the string with three quetes in-a-row, rather than just one
print('''the
big
black
box''')