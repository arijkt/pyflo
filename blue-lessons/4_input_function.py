# declares a variable and assigns it an empty string value
address = ''

# type an address and assigns the user's input to the variable
output = input('Type an address: ' + address)

# prints the value of a variable along with a fixed string
print('The address you types is: ' + output)

character_1 = input('Name for character 1: ')
character_2 = input('Name for character 2: ')
print(character_1 + ': Hi ' + character_2 + ', long time since we talked last.')
print(character_2 + ': Hi ' + character_1 + '! How have you been?')
print(character_1 + ': I have been well. Do you wanna go get lunch?')
print(character_2 + ': Yes!')
print(character_1 + ': Great, we can meet at In-n-Out burger next week')

print('  *')
print(' ***')
print('*****')
print('  |  ')

letter = input('Type a letter to use for the Christmas tree: ')
print('  ' + letter)
print(' ' + letter + letter + letter)
print(letter + letter + letter + letter + letter)
print('  |  ')