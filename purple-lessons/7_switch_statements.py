age = 120

if age > 90:
    print('You\'re too old to party, granny')
elif age < 0:
    print('You\'re yet to be born')
elif age >= 18:
    print('You are allowed to party')

def switch(lang):
    if lang == "JS":
        return "You can become a web developer"
    elif lang == "PHP":
        return "You can become a backend developer"
    elif lang == "PY":
        return "You can become data Scientist"
    elif lang == "Solidity":
        return "You can become a Blockchain developer"
    elif lang == "Java":
        return "You can become a mobile app developer"

print(switch("JS"))
print(switch("PHP"))
print(switch("Java"))

lang = input("What is the programming language you want to learn? ")

match lang:
    case "JS":
        print("You can become a web developer")
    case "PY":
        print("You can become a data Scientist")
    case "Java":
        print("you can become a mobile app developer")
    case _:
        print("The language does not matter, what matters is solving problems.")