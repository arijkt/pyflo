# enter the business revenue and stores it as an integer in the variable
revenue = int(input('Business revenue: '))
# enter the business revenue and stores it as an integer in the variable
cost = int(input('Business costs: '))
profit = revenue - cost
print('The business profit is: ' + str(profit))

# without int() the proble is that python dosen't support string multiplication with floats!
# if you wanna use string multiplication, ensure that you're multiplying the string by an integer, not a float
cost_bar = '#' * int((cost / revenue) * 25)
profit_bar = '#' * int((profit / revenue) * 25)
print('  cost: ' + cost_bar)
print('profit: ' + profit_bar)