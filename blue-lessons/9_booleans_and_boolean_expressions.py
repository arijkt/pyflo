# True or False (the capitalisation is important!)
the_sky_is_blue = True
food_is_bad = False
print(the_sky_is_blue, food_is_bad)

enough_money = 1000 > 100
do_the_names_match = 'Ryan' == 'Rich'
print(enough_money, do_the_names_match)

'''
A < B result in True if A is less than B, False otherwise.
A <= B results in True if A is less than or equal to B, False otherwise.
A > B results in True if A is greater than B, False otherwise.
A >= B results in True if A is greater than or equal to B, False otherwise.
A == B results in True if A is equal to B, False otherwise.
A != B results in True if A is not equal to B, False otherwise.
'''

home_cost = int('How much does the home cost? ')
available_funds = int(input('How much money do you have? '))
down_payment = home_cost * 0.2
if available_funds >= down_payment:
    print('You can afford the down payment!')
    
name = input('What is your name? ')
if name == 'Bob':
    print('Hello, Bob!')
else:
    print('Hello, ' + name + '!')