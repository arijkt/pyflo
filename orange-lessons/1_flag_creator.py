# implenet the code to print out the 10x4 flag
# this should take no more than 4 lines of code
flag_height = 10
flag_width = 4
first_2_rows = '#' * int((flag_height / 2)) + '-' * int(flag_height / 2)
last_2_lines = '-' * int(flag_width * 2 + 2)

print(first_2_rows + '\n' + first_2_rows + '\n' + last_2_lines + '\n' + last_2_lines + '\n')

# implement the code to print out a flag that can vary in width only
width = input("Flag width:\n")
width = int(width)
first_2_rows = '#' * int((width / 2)) + '-' * int(width / 2)
last_2_lines = '-' * int(width)

print(first_2_rows + '\n' + first_2_rows + '\n' + last_2_lines + '\n' + last_2_lines + '\n')

# implement the code to print out a flag that can vary in both width and height. It should behave the same way that the two examples earlier in this project showed
width = input("Flag width:\n")
width = int(width)
height = input("Flag height:\n")
height = int(height)

half_width = int(width / 2)
half_height = int(height / 2)

upper_row = '#' * half_width + '-' * half_width + '\n'
lower_row = '-' * width + '\n'

print(upper_row * half_height, end='')
print(lower_row * half_height, end='')