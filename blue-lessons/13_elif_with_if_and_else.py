# An Example: Register for Class
experience_level = input("What is your current experience level in Spanish? ")
if experience_level.lower() == "none":
    print("We recommend you take Spanish 101.")
elif experience_level.lower() in ["101", "102", "103"]:
    print("We recommend you take the next course in the sequence.")
elif experience_level.lower() == "202":
    print("We recommend you move on to advanced courses.")
