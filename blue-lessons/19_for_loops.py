word = input('Give me a word: ')
first_last = word[0] + word[-1]
print('Here\'s the first and last letter', first_last)

print('before')
for i in range(1, 5):
    print('hi')
    print('i variable is:', str(i))
print('after')

for count in range(0, 4):
    word = input('Give me a word: ')
    first_last = word[0] + word[-1]
    print('Here\'s the first and last letter', first_last)

# loop tersebut akan mengulangi kode tersebut empat kali, dan kemudian program akan berakhir.  
# Jika kebutuhan kita berubah, 
# dan tiba-tiba kita perlu mengulangi kode ini 10 kali, 
# bukan empat kali, 
# yang diperlukan hanyalah satu perubahan kecil.