# Here’s an example from everyday life pertaining to eating and drinking:
am_i_hungry = input('Am I hungry? (y/n) ')

def fuck_nested_if():
    am_i_thirsty = input('Am I Thirsty? (y/n)? ')
    if am_i_thirsty.lower() == 'y':
        drink_choice = input('What should I drink? ')
        if drink_choice.lower() in ['drink water', 'drink juice']:
            print('Keep working')
    else:
        print('Keep working')

if am_i_hungry.lower() == 'y':
    eat_choice = input('Should I eat? ')
    if eat_choice.lower() in ['eat pizza', 'eat a burger', 'eat a salad']:
        fuck_nested_if()
    else:
        fuck_nested_if()
else:
    fuck_nested_if()
    
    
# For example, the code below, though completely made-up and not very useful, is also totally valid python:
x = int(input('What is x? '))
y = int(input('What is y? '))

if x > y:
    if y * 2 == x:
        if y < x - 20:
            if y == 100:
                print('I reached this line!')
            else:
                print('Else A')
        else:
            print('Else B')
    else:
        print('Else C')
else:
    print('Else D')