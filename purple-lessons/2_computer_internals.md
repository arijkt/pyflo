# Computer Internals

Modern computers share a common set of hardware components, varying depending on their purpose. For example, a thermostat controlling air conditioning has a basic CPU and memory, but sacrifices in computational power for smaller, energy-efficient hardware. Larger computers, like 3D graphics or video editing, are larger but more capable, with similar size, power, capability, and energy efficiency. Core components remain similar across various devices.

At an abstracted level, the components of a computer can be summarized by the following diagram:

![dagram](https://pyflo.net/computer-internals/computer.png)

Computers are input/output devices that receive input from humans and produce outputs. Inputs can be keyboard typing, mouse clicks, touchscreen presses, video camera feeds, or environmental sensors. Processing (CPU, RAM) takes this input and produces outputs like text, graphics, and sounds. This lesson aims to explain how computer components function and interact.

# The Central Processing Unit

The CPU, also known as the "brain" of a computer, executes programs and processes simple instructions from programmers. It can perform basic math calculations and data movement. Modern CPUs can execute over one billion instructions per second, making them capable of processing various applications like email, social media, homework, and Python code. However, to function properly, a computer requires additional hardware, such as a memory to track programs and data. Despite its powerful capabilities, the CPU requires additional hardware to function properly, making it a crucial component of a computer's functioning.

# Random-Access Memory

The CPU, also known as the "brain" of a computer, executes programs and processes simple instructions from programmers. It can perform basic math calculations and data movement. Modern CPUs can execute over one billion instructions per second, making them capable of processing various applications like email, social media, homework, and Python code. However, to function properly, a computer requires additional hardware, such as a memory to track programs and data. Despite its powerful capabilities, the CPU requires additional hardware to function properly, making it a crucial component of a computer's functioning.

# The Hard Drive

Hard drives are memory forms that can be remembered even when a computer is turned off, unlike RAM which is short-term memory that is cleared every time a computer is restarted. There are various types of hard drives, including Solid-State Disks (SSDs) and Hard Drive Disks (HDDs). SSDs use electronics to store binary data, while HDDs use spinning disks and magnetic needles. SSDs are faster and more resistant to damage from dropped or mishandled devices. A hard drive is crucial for computer functionality, as it allows for long-term storage of programs, files, and operating systems.

# The Motherboard

The motherboard is a crucial component in a computer, tying together various components like the CPU, memory, and hard drive. It facilitates communication and maintains sync with other input and output devices like the mouse, keyboard, monitor, and speakers, making it a critical "facilitator."

# Conclusion

There are other components internal to some computer systems such as graphics cards and WNICs (Wireless Network Interface Cards) that we will not cover in depth here. For now, having a foundational understanding of the “core 4” components (CPU, RAM, hard drive, motherboard) as well as input and output devices is sufficient. The idea with this section is not to make you computer hardware experts. Rather, we want you to know just enough to assist with your understanding of computer programming and python. However, if diving into the details of this componentry piques your interest, feel free to take a deep dive into computer architecture elsewhere.

[cd ..](./README.md)
