city = 'Jerusalem'
letter = city[4]
print(letter)

# start from 0 rather than 1
rock = city[0] + city[1] + city[8]
print(rock)

# start from -1 rather than 0
rock_reverse = city[-9] + city[-8] + city[-1]
print(rock_reverse)

text = input('Type something: ')
print('The last two characters of your input were: ' + text[-2] + text[-1])

last = text[len(text) - 1]
other = text[len(text) - 2]
print('The last two characters of your input were: ' + last + other)