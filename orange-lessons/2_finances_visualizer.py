# Get the 5 input values from the user
print('--------------------------------')
print('----- FINANCIAL VISUALIZER -----')
print('--------------------------------')

salary = input('Annual Salary: ')
housing = input('Monthly Housing: ')
bills = input('Monthly Bills: ')
food = input('Weekly Food: ')
travel = input('Annual Travel: ')

# Check if the 5 input values can be converted to a floating-point number
# If so, convert them
def isinvalid(x):
    for i in x:
        if i.isnumeric() == False and i != '.':
            return True
    return False

if isinvalid(salary or housing or bills or food or travel):
    print('Invalid input, please try again!')
else:
    print('All input confirmed valid.')

salary = float(salary) 
housing = float(housing)
bills = float(bills)
food = float(food)
travel = float(travel)

# Compute the annual tax
annual_tax = 0

if salary < 10000:
    annual_tax = round(salary * 0.05, 2)
elif salary < 40000:
    annual_tax = round(salary * 0.1, 2)
elif salary < 80000:
    annual_tax = round(salary * 0.15, 2)
else:
    annual_tax = round(salary * 0.20, 2)

# Calculate the annual dollar amounts and the annual percentages
annual_housing = housing * 12
annual_bills = bills * 12
annual_food = food * 52
annual_travel = travel
annual_extra = salary - annual_housing - annual_bills - annual_food - annual_travel - annual_tax

percentage_tax = round((annual_tax / salary) * 100, 1)
percentage_housing = round((annual_housing / salary) * 100, 1)
percentage_bills = round((annual_bills / salary) * 100, 1)
percentage_food = round((annual_food / salary) * 100, 1)
percentage_travel = round((annual_travel / salary) * 100, 1)
percentage_extra = round((annual_extra / salary) * 100, 1)

# Display the grid with financial information and bar chart
width = int(min(
    percentage_tax,
    percentage_housing,
    percentage_bills,
    percentage_food,
    percentage_travel,
    percentage_extra
))

calc_percentage_housing = int(percentage_housing * 20 / 100)
calc_percentage_bills = int(percentage_bills * 20 / 100)
calc_percentage_food = int(percentage_food * 20 / 100)
calc_percentage_travel = int(percentage_travel * 20 / 100)
calc_percentage_tax = int(percentage_tax * 20 / 100)
calc_percentage_extra = int(percentage_extra * 20 / 100 )

hash_percentage_housing = min(calc_percentage_housing, 20)
hash_percentage_bills = min(calc_percentage_bills, 20)
hash_percentage_food = min(calc_percentage_food, 20)
hash_percentage_travel = min(calc_percentage_travel, 20)
hash_percentage_tax = min(calc_percentage_tax, 20)
hash_percentage_extra = min(calc_percentage_extra, 20)

boundary = '----------------------------------' + ('-' * width)
print()
print(boundary)

print('housing | $' + format(annual_housing, '11,.2f') + ' ', end='')
print('| ' + format(percentage_housing, '5,.1f') + '% | ' + ('#' * hash_percentage_housing))
print('  bills | $' + format(annual_bills, '11,.2f') + ' ', end='')
print('| ' + format(percentage_bills, '5,.1f') + '% | ' + ('#' * hash_percentage_bills))
print('   food | $' + format(annual_food, '11,.2f') + ' ', end='')
print('| ' + format(percentage_food, '5,.1f') + '% | ' + ('#' * hash_percentage_food))
print(' travel | $' + format(annual_travel, '11,.2f') + ' ', end='')
print('| ' + format(percentage_travel, '5,.1f') + '% | ' + ('#' * hash_percentage_travel))
print('    tax | $' + format(annual_tax, '11,.2f') + ' ', end='')
print('| ' + format(percentage_tax, '5,.1f') + '% | ' + ('#' * hash_percentage_tax))
print('  extra | $' + format(annual_extra, '11,.2f') + ' ', end='')
print('| ' + format(percentage_extra, '5,.1f') + '% | ' + ('#' * hash_percentage_extra))
print(boundary)