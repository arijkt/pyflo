# is letting the computer know that we're gonna need to get some information about time
import time

# asks the computer what the current year's and saves it by the name current_year
current_year = int(time.strftime("%Y"))
# asks whoever is using this program to enter when he/she got the passport
recieved_year = int(input('What year did you get your passoport? '))

# if the recieved year is less than the current year
if recieved_year + 10 < current_year:
    # print that you should get a new passport
    print('You should go get a new passport')
else:
    # print that you may use your passport
    print('You may use your passport')