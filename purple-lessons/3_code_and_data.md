# Code and Data

Computers use memory for various purposes, including storing code or computer programs. These programs are sequences of instructions designed to be executed by a CPU. They need storage in the long-term storage of the hard drive and in the short-term memory of the RAM. The computer's memory and storage are divided into code and data categories.

Computers store data, not just code, to perform useful tasks. Programs like Adobe Photoshop and Microsoft Word require input data to function effectively. This data is stored in various formats, such as.jpeg,.png,.gif, and.ppm, and.docx,.txt, and.rtf. These formats represent various types of data, such as images (.jpeg,.png,.gif, and.ppm) and text (.docx,.txt, and.rtf). As knowledge of computers and programming grows, more formats will be covered in future lessons. Overall, storing data on hard drives consumes significant space.

[cd ..](./README.md)